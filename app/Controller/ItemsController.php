<?php
class ItemsController extends AppController {
	public $scaffold;
	
	public $helpers = array('Js', 'Html', 'Form', 'Session');
	public $components = array('RequestHandler');
	
	var $filters = array();
	var $filterValues = array();

    function index() {
		
		$data = $this->Item->find('all');
		
		//Gets an array of filters by Item columns
		$filters = $this->getFilters();
			
		//Set all filters to non active state
		foreach ($filters as $key => $values) {
			foreach ($values as $value) {
				$this->filterValues[$key][$value['Item'][$key]] = array('active' => 0);								
			}
		}
		
		//Saves filter values to a session
		$this->Session->write('filterValues', $this->filterValues);
	
		$this->set('filters', $filters);
		$this->set('filterValues', $this->Session->read('filterValues'));
		$this->set('items', $data);
		
	}

	function filterItems() {

		if ($this->request->is('ajax')) {
			if (!empty($this->request->data)) {
			    $this->autoRender = false;
	        	$this->layout = 'ajax';	
							
			 	$filterName = key($this->request->data);
				$filterType = key($this->request->data[$filterName]);
					
				//Changing filter states
				$sessionFilters= $this->Session->read('filterValues');
				if ($sessionFilters[$filterName][$filterType]['active'] == 0) {
					$sessionFilters[$filterName][$filterType]['active'] = 1;
				} else {
					$sessionFilters[$filterName][$filterType]['active'] = 0;
				}
					
				//Updating filter states values
				$this->Session->write('filterValues', $sessionFilters);
					
				//Getting current values
				$currentValues = $this->Session->read('filterValues');
				$filters = $this->getFilters();					
	
				//Setting up a conditions array with all active filters				
				$conditions = array();
				foreach ($filters as $key=>$values) {
					$conditions[$key] = array();
					foreach ($values as $value) {
						if ($currentValues[$key][$value['Item'][$key]]['active'] == 1) {
					  		array_push($conditions[$key], $value['Item'][$key]);				
						}
					}
					if (empty($conditions[$key])) { unset($conditions[$key]); }
				}
						
				//Selecting data with filter conditions
				$data = $this->Item->find('all', array(
					'conditions' => $conditions
				));
					
			    $this->set('items', $data);
				$this->set('filters', $filters);		
				$this->set('filterValues', $this->Session->read('filterValues'));		
				$this->render('index', 'ajax');
			}
		 }
		
	}
	
    function getFilters() {
		$brands = $this->Item->find('all', array(
			'fields'=>array('DISTINCT Item.brand'), 
			'order'=>array('Item.brand DESC')
		));
		
		$colors = $this->Item->find('all', array(
			'fields'=>array('DISTINCT Item.color'), 
			'order'=>array('Item.color DESC')
		));
		
		$filters = array(
			'brand' => $brands, 
			'color' => $colors
		);
		
		return $filters;
	}

}
