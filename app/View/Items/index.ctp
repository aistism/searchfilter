<div id="browseItems" class="row-fluid">
	
	<div id="filters" class="span2" >
	<?php foreach ($filters as $filter=>$types): ?> <!-- Prints checkboxes grouped by filters -->
		<div class="filterBox">
			<span><?php echo 'Filter by ' . $filter; ?><div class="filterToggle"></div></span>

			<div class="filterList">
			<?php echo $this->Form->create($filter); ?>				
			<?php foreach ($types as $type): ?>		
				<div class="filterSelector">
					<label>
						<?php 
							$check = false;
							if ($filterValues[$filter][$type['Item'][$filter]]['active'] == 1) {
								$check = true;
							}
							echo $this->Form->checkbox($type['Item'][$filter], array('checked' => $check)); 
						?>
						<?php echo $type['Item'][$filter]; ?>

					</label> 
					
				</div>
			<?php endforeach; ?>
			<?php echo $this->Form->end(); ?>	
			</div>
		</div>
	<?php endforeach; ?>
	</div>
	
	<div class="span10">
		<table id="itemsTable">
		    <tr>
		        <th>Name</th>
		        <th>Brand</th>
		        <th>Price</th>
		        <th>Color</th>
		        <th>Description</th>
		    </tr>
		
		    <?php foreach ($items as $item): ?>
		    <tr>
				<td><?php echo $item['Item']['name']; ?></td>
		        <td><?php echo $item['Item']['brand']; ?></td>
		        <td><?php echo $item['Item']['price']; ?></td>
		        <td><?php echo $item['Item']['color']; ?></td>
		        <td><?php echo $item['Item']['description']; ?></td>
		    </tr>
		    <?php endforeach; ?>
		    <?php unset($item); ?>	
		    
	    	<?php echo $this->Html->image('loader.gif', array('id' => 'loaderGif')); ?>

		</table>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#loaderGif').hide();
		$("#filters .filterBox span").hover(function(){
			$(this).css("cursor", "pointer");
		});
		
		$("#filters .filterBox span").click(function(){
			var list = $(this).parent().find(".filterList");
			list.toggle();
		});
		
		
	});
</script>
<?php 

$this->Js->get('.filterSelector input');
$this->Js->event(
    'click', 

    $this->Js->request(
        array('controller' => 'items', 'action' => 'filterItems'),
        array(
        	'async' => true,
	        'method' => 'post', 
	        'dataExpression'=> true , 
	        'update' => '#browseItems', 
	 		'before' => "$('#loaderGif').show();",
	 		'success' => "$('#loaderGif').show();",
	        'data' => '$(this)'  

		)
    )

);	

?>

<?php echo $this->Js->writeBuffer(); ?>
